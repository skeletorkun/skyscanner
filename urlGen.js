var moment = require('moment');
var openr = require('opener');

var start, end, duration, url;
var flex = 3;
var sampleUrl = "https://www.skyscanner.fr/transport/vols/nce/ista/START/END/tarifs-de-nice-a-istanbul-en-juillet-2016.html?adults=1&children=0&infants=0&cabinclass=economy&rtn=1&preferdirects=true&outboundaltsenabled=false&inboundaltsenabled=false#results";

var links = [];

var avgRating = 3.20;
	 
var run = function(){

	if(checkArgs())
	{		
		fillLinks();		 
		printArray(links);
		//openInBrowser(links);
	}
 };
 
 var fillLinks = function(){
	  
	console.log('Starting to create urls for a (+-' + flex + ') ' + duration + ' days trip.');		
	
	var searchStartDate = moment(start, "YYMMDD");  // searchStartDate = 1 March
	var searchEndDate = moment(end, "YYMMDD");		// searchEndDate = 30 April
	
	// init 
	var tripStart = searchStartDate.clone(); // tripStart = 1 March
	var tripEnd = searchStartDate.clone();   // tripEnd = 1 March
	tripEnd.add(duration, 'day');			 // tripEnd = 1 + 20 March
		
	while(tripEnd.isBefore(searchEndDate))
	{		
		var flexyEndMin = tripEnd.clone().subtract(flex, 'day'); // flexyEndMin = 20 - 3 = 17 March
		var flexyEndMax = tripEnd.clone().add(flex, 'day');		 // flexyEndMax = 20 + 3 = 23 March
		
		// loop while start is constant and increment end date from 17 march to 23 march
		while(flexyEndMin.isBefore(flexyEndMax)) 
		{			
			console.log('tripStart ' + tripStart.toString() + ' flexyEndMin ' + flexyEndMin.toString());
			var rating = rateDates(tripStart.clone(), flexyEndMin.clone());
			if(rating > avgRating)
			{
				addLinkForDates(tripStart, flexyEndMin);
			}
			else
			{
				console.log('skipped');
			}
			
			flexyEndMin.add(1, 'day');
		}
			
		
		tripStart.add(1, 'day');
		tripEnd.add(1, 'day');		
	}
 };
 
 var addLinkForDates = function(s, e){
	var sStr = s.format('YYMMDD');
	var eStr = e.format('YYMMDD');
	var sDoW = getDoWStr(s);
	var eDow = getDoWStr(e);
	links.push(sDoW + ' to ' + eDow + " : " + url.replace("START", sStr).replace("END", eStr));			
 }
 
 var checkArgs = function(){
	 
	if(process.argv.length < 3){
		console.log('example usage is npm start ' + sampleUrl + ' 161130 161230 20 3' );		
		console.log('Meaning "any 20 (+-3) days between 30 Nov and 30 Dec "');
		return false;
	}

	// print process.argv
	process.argv.forEach(function (val, index, array) {  
	  if(index == 2) 
	  {
		  url = val;
		  console.log('url : ' + val);
	  }
	  else if(index == 3)
	  {
		  start = val;
		  console.log('start date : ' + val);
	  }
	  else if(index == 4)
	  {
		  end = val;
		  console.log('end date : ' + val);
	  }
	  else if(index == 5)
	  {
		  duration = val;
		  console.log('duration: ' + val);
	  }
	  else if(index == 6)
	  {
		  flex = val;
	  }
	});
			
	console.log('suggestions will have +-' + flex + ' days');
	return true;
 };
 
 var rateDates = function(start, end){	 
	
	var num = 0;
	var points = 0;
	while(start.isBefore(end))
	{
		points += rateDayOfWeek(start);
		start.add(1,'day');
		num++;
	}
	
	var rating = points / num;
	console.log('Rating : ' + rating)
	return rating;
 }
 
 var openInBrowser = function(array){
	
	if(array.length > 10){
		console.log('too many links!');
	}
	
	array.forEach(function (l) { 
		openr(l);
	});	
 };
 
 function getDoWStr(day)
 {
	 switch(day.day())
	 {
		case 0: return 'Sunday';
		case 1: return 'Monday';
		case 2: return 'Tuesday';
		case 3: return 'Wednesday';
		case 4: return 'Thursday';
		case 5: return 'Friday';
		case 6: return 'Saturday';
	 }
 }
 
 function rateDayOfWeek(day){
	 switch(day.day())
	 {
		case 0: //sun
		case 6: //sat
			return 5;
		case 1:
		case 5:
			return 3;
		case 2:
		case 4:
			return 2;
		case 3: //wed
			return 1; 
	 }
 }

function dateDiffInDays(a, b) {	
	return a.diff(b,'days');
}

function printArray(array){
	array.forEach(function (l) { 
		console.log(l);
	});	
};


run();